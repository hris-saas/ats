<?php

namespace HRis\ATS\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Applicant extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'uuid', 'first_name', 'middle_name', 'last_name', 'resume_cv', 'job_title_id', 'status_id', 'type_id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * A model may have multiple statuses.
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(Applicant\Status::class);
    }

    /**
     * A model may have multiple types.
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(Applicant\Type::class);
    }
}
