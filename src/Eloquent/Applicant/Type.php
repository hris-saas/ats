<?php

namespace HRis\ATS\Eloquent\Applicant;

use HRis\Core\Eloquent\Type as Model;

class Type extends Model
{
    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = ['class' => self::class];
}
