<?php

namespace HRis\ATS\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Step extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'slug', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * A model may have multiple statuses.
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(Step\Status::class);
    }
}
