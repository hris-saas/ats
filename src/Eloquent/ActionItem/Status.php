<?php

namespace HRis\ATS\Eloquent\ActionItem;

use HRis\PIM\Eloquent\Status as Model;

class Status extends Model
{
    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = ['class' => self::class];
}
