<?php

namespace HRis\ATS\Eloquent\ActionItem;

use HRis\Core\Eloquent\Type as Model;

class Type extends Model
{
    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = ['class' => self::class];
}
