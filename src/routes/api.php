<?php

use Illuminate\Support\Facades\Route;
use HRis\ATS\Http\Controllers\StepController;

// guest
Route::group(['middleware' => 'guest:api'], function () {
    //
});

// auth
Route::group(['middleware' => ['auth:api']], function () {
    Route::get('steps', [StepController::class, 'index'])->name('step.index');                               // postman
    Route::get('steps/{step}', [StepController::class, 'show'])->name('step.show');                          // postman
});
