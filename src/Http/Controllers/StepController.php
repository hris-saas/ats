<?php

namespace HRis\ATS\Http\Controllers;

use HRis\ATS\Eloquent\Step;
use HRis\ATS\Http\Resources\Step as Resource;
use HRis\ATS\Http\Requests\StepRequest as Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class StepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $steps = Step::paginate($this->perPage);

        return Resource::collection($steps);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Step    $step
     *
     * @return Resource
     */
    public function show(Request $request, Step $step): Resource
    {
        return new Resource($step);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StepRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|\HRis\ATS\Http\Resources\Step
     */
    public function store()
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StepRequest $request
     * @param Step        $step
     *
     * @return \HRis\ATS\Http\Resources\Step
     */
    public function update()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param StepRequest $request
     * @param Step        $step
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy()
    {
    }
}
